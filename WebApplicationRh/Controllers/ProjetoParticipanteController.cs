﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebApplicationRh;
using WebApplicationRh.Models;

namespace WebApplicationRh.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProjetoParticipanteController : ApiController
    {
        private RhEntityDataModel db = new RhEntityDataModel();

        // GET: api/ProjetoParticipantes
        public IQueryable<ProjetoParticipantes> GetProjetoParticipantes()
        {
            return db.ProjetoParticipantes;
        }

        // GET: api/ProjetoParticipantes/5
        [ResponseType(typeof(ProjetoParticipantes))]
        public async Task<IHttpActionResult> GetProjetoParticipantes(int id)
        {
            ProjetoParticipantes projetoParticipantes = await db.ProjetoParticipantes.FindAsync(id);
            if (projetoParticipantes == null)
            {
                return NotFound();
            }

            return Ok(projetoParticipantes);
        }

        // PUT: api/ProjetoParticipantes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProjetoParticipantes(int id, ProjetoParticipantes projetoParticipantes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != projetoParticipantes.ProjetoId)
            {
                return BadRequest();
            }

            db.Entry(projetoParticipantes).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProjetoParticipantesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProjetoParticipantes
        [ResponseType(typeof(ProjetoParticipantes))]
        public async Task<IHttpActionResult> PostProjetoParticipantes(ProjetoParticipantes projetoParticipantes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ProjetoParticipantes.Add(projetoParticipantes);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProjetoParticipantesExists(projetoParticipantes.ProjetoId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = projetoParticipantes.ProjetoId }, projetoParticipantes);
        }

        // DELETE: api/ProjetoParticipantes/5
        [ResponseType(typeof(ProjetoParticipantes))]
        public async Task<IHttpActionResult> DeleteProjetoParticipantes(int id)
        {
            ProjetoParticipantes projetoParticipantes = await db.ProjetoParticipantes.FindAsync(id);
            if (projetoParticipantes == null)
            {
                return NotFound();
            }

            db.ProjetoParticipantes.Remove(projetoParticipantes);
            await db.SaveChangesAsync();

            return Ok(projetoParticipantes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProjetoParticipantesExists(int id)
        {
            return db.ProjetoParticipantes.Count(e => e.ProjetoId == id) > 0;
        }
    }
}