﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebApplicationRh;
using WebApplicationRh.Models;

namespace WebApplicationRh.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PessoaHabilidadeRecomendacaoController : ApiController
    {
        private RhEntityDataModel db = new RhEntityDataModel();

        // GET: api/PessoaHabilidadeRecomendacoes
        public IQueryable<PessoaHabilidadeRecomendacoes> GetPessoaHabilidadeRecomendacoes()
        {
            return db.PessoaHabilidadeRecomendacoes;
        }

        // GET: api/PessoaHabilidadeRecomendacoes/5
        [ResponseType(typeof(PessoaHabilidadeRecomendacoes))]
        public async Task<IHttpActionResult> GetPessoaHabilidadeRecomendacoes(int id)
        {
            PessoaHabilidadeRecomendacoes pessoaHabilidadeRecomendacoes = await db.PessoaHabilidadeRecomendacoes.FindAsync(id);
            if (pessoaHabilidadeRecomendacoes == null)
            {
                return NotFound();
            }

            return Ok(pessoaHabilidadeRecomendacoes);
        }

        // PUT: api/PessoaHabilidadeRecomendacoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPessoaHabilidadeRecomendacoes(int id, PessoaHabilidadeRecomendacoes pessoaHabilidadeRecomendacoes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pessoaHabilidadeRecomendacoes.Id)
            {
                return BadRequest();
            }

            db.Entry(pessoaHabilidadeRecomendacoes).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PessoaHabilidadeRecomendacoesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PessoaHabilidadeRecomendacoes
        [ResponseType(typeof(PessoaHabilidadeRecomendacoes))]
        public async Task<IHttpActionResult> PostPessoaHabilidadeRecomendacoes(PessoaHabilidadeRecomendacoes pessoaHabilidadeRecomendacoes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PessoaHabilidadeRecomendacoes.Add(pessoaHabilidadeRecomendacoes);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PessoaHabilidadeRecomendacoesExists(pessoaHabilidadeRecomendacoes.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pessoaHabilidadeRecomendacoes.Id }, pessoaHabilidadeRecomendacoes);
        }

        // DELETE: api/PessoaHabilidadeRecomendacoes/5
        [ResponseType(typeof(PessoaHabilidadeRecomendacoes))]
        public async Task<IHttpActionResult> DeletePessoaHabilidadeRecomendacoes(int id)
        {
            PessoaHabilidadeRecomendacoes pessoaHabilidadeRecomendacoes = await db.PessoaHabilidadeRecomendacoes.FindAsync(id);
            if (pessoaHabilidadeRecomendacoes == null)
            {
                return NotFound();
            }

            db.PessoaHabilidadeRecomendacoes.Remove(pessoaHabilidadeRecomendacoes);
            await db.SaveChangesAsync();

            return Ok(pessoaHabilidadeRecomendacoes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PessoaHabilidadeRecomendacoesExists(int id)
        {
            return db.PessoaHabilidadeRecomendacoes.Count(e => e.Id == id) > 0;
        }
    }
}