﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebApplicationRh;
using WebApplicationRh.Models;

namespace WebApplicationRh.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CargoController : ApiController
    {
        private RhEntityDataModel db = new RhEntityDataModel();

        // GET: api/Cargoes
        public IQueryable<Cargo> GetCargo()
        {
            return db.Cargo;
        }

        // GET: api/Cargoes/5
        [ResponseType(typeof(Cargo))]
        public async Task<IHttpActionResult> GetCargo(int id)
        {
            Cargo cargo = await db.Cargo.FindAsync(id);
            if (cargo == null)
            {
                return NotFound();
            }

            return Ok(cargo);
        }

        // PUT: api/Cargoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCargo(int id, Cargo cargo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cargo.Id)
            {
                return BadRequest();
            }

            db.Entry(cargo).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CargoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Cargoes
        [ResponseType(typeof(Cargo))]
        public async Task<IHttpActionResult> PostCargo(Cargo cargo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Cargo.Add(cargo);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CargoExists(cargo.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = cargo.Id }, cargo);
        }

        // DELETE: api/Cargoes/5
        [ResponseType(typeof(Cargo))]
        public async Task<IHttpActionResult> DeleteCargo(int id)
        {
            Cargo cargo = await db.Cargo.FindAsync(id);
            if (cargo == null)
            {
                return NotFound();
            }

            db.Cargo.Remove(cargo);
            await db.SaveChangesAsync();

            return Ok(cargo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CargoExists(int id)
        {
            return db.Cargo.Count(e => e.Id == id) > 0;
        }
    }
}