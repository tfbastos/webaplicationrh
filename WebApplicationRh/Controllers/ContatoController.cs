﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebApplicationRh;
using WebApplicationRh.Models;

namespace WebApplicationRh.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ContatoController : ApiController
    {
        private RhEntityDataModel db = new RhEntityDataModel();

        // GET: api/Contatos
        public IQueryable<Contatos> GetContatos()
        {
            return db.Contatos;
        }

        // GET: api/Contatos/5
        [ResponseType(typeof(Contatos))]
        public async Task<IHttpActionResult> GetContatos(int id)
        {
            Contatos contatos = await db.Contatos.FindAsync(id);
            if (contatos == null)
            {
                return NotFound();
            }

            return Ok(contatos);
        }

        // PUT: api/Contatos/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutContatos(int id, Contatos contatos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != contatos.Id)
            {
                return BadRequest();
            }

            db.Entry(contatos).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContatosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Contatos
        [ResponseType(typeof(Contatos))]
        public async Task<IHttpActionResult> PostContatos(Contatos contatos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Contatos.Add(contatos);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ContatosExists(contatos.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = contatos.Id }, contatos);
        }

        // DELETE: api/Contatos/5
        [ResponseType(typeof(Contatos))]
        public async Task<IHttpActionResult> DeleteContatos(int id)
        {
            Contatos contatos = await db.Contatos.FindAsync(id);
            if (contatos == null)
            {
                return NotFound();
            }

            db.Contatos.Remove(contatos);
            await db.SaveChangesAsync();

            return Ok(contatos);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ContatosExists(int id)
        {
            return db.Contatos.Count(e => e.Id == id) > 0;
        }
    }
}