﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebApplicationRh;
using WebApplicationRh.Models;

namespace WebApplicationRh.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CertificacaoController : ApiController
    {
        private RhEntityDataModel db = new RhEntityDataModel();

        // GET: api/Certificacoes
        public IQueryable<Certificacoes> GetCertificacoes()
        {
            return db.Certificacoes;
        }

        // GET: api/Certificacoes/5
        [ResponseType(typeof(Certificacoes))]
        public async Task<IHttpActionResult> GetCertificacoes(int id)
        {
            Certificacoes certificacoes = await db.Certificacoes.FindAsync(id);
            if (certificacoes == null)
            {
                return NotFound();
            }

            return Ok(certificacoes);
        }

        // PUT: api/Certificacoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCertificacoes(int id, Certificacoes certificacoes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != certificacoes.Id)
            {
                return BadRequest();
            }

            db.Entry(certificacoes).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CertificacoesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Certificacoes
        [ResponseType(typeof(Certificacoes))]
        public async Task<IHttpActionResult> PostCertificacoes(Certificacoes certificacoes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Certificacoes.Add(certificacoes);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CertificacoesExists(certificacoes.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = certificacoes.Id }, certificacoes);
        }

        // DELETE: api/Certificacoes/5
        [ResponseType(typeof(Certificacoes))]
        public async Task<IHttpActionResult> DeleteCertificacoes(int id)
        {
            Certificacoes certificacoes = await db.Certificacoes.FindAsync(id);
            if (certificacoes == null)
            {
                return NotFound();
            }

            db.Certificacoes.Remove(certificacoes);
            await db.SaveChangesAsync();

            return Ok(certificacoes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CertificacoesExists(int id)
        {
            return db.Certificacoes.Count(e => e.Id == id) > 0;
        }
    }
}