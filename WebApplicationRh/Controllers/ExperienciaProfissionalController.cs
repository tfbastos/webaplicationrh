﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebApplicationRh;
using WebApplicationRh.Models;

namespace WebApplicationRh.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ExperienciaProfissionalController : ApiController
    {
        private RhEntityDataModel db = new RhEntityDataModel();

        // GET: api/ExperienciaProfissionals
        public IQueryable<ExperienciaProfissional> GetExperienciaProfissional()
        {
            return db.ExperienciaProfissional;
        }

        // GET: api/ExperienciaProfissionals/5
        [ResponseType(typeof(ExperienciaProfissional))]
        public async Task<IHttpActionResult> GetExperienciaProfissional(int id)
        {
            ExperienciaProfissional experienciaProfissional = await db.ExperienciaProfissional.FindAsync(id);
            if (experienciaProfissional == null)
            {
                return NotFound();
            }

            return Ok(experienciaProfissional);
        }

        // PUT: api/ExperienciaProfissionals/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutExperienciaProfissional(int id, ExperienciaProfissional experienciaProfissional)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != experienciaProfissional.Id)
            {
                return BadRequest();
            }

            db.Entry(experienciaProfissional).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExperienciaProfissionalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ExperienciaProfissionals
        [ResponseType(typeof(ExperienciaProfissional))]
        public async Task<IHttpActionResult> PostExperienciaProfissional(ExperienciaProfissional experienciaProfissional)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ExperienciaProfissional.Add(experienciaProfissional);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ExperienciaProfissionalExists(experienciaProfissional.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = experienciaProfissional.Id }, experienciaProfissional);
        }

        // DELETE: api/ExperienciaProfissionals/5
        [ResponseType(typeof(ExperienciaProfissional))]
        public async Task<IHttpActionResult> DeleteExperienciaProfissional(int id)
        {
            ExperienciaProfissional experienciaProfissional = await db.ExperienciaProfissional.FindAsync(id);
            if (experienciaProfissional == null)
            {
                return NotFound();
            }

            db.ExperienciaProfissional.Remove(experienciaProfissional);
            await db.SaveChangesAsync();

            return Ok(experienciaProfissional);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ExperienciaProfissionalExists(int id)
        {
            return db.ExperienciaProfissional.Count(e => e.Id == id) > 0;
        }
    }
}