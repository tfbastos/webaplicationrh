﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebApplicationRh;
using WebApplicationRh.Models;

namespace WebApplicationRh.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProjetoController : ApiController
    {
        private RhEntityDataModel db = new RhEntityDataModel();

        // GET: api/Projetoes
        public IQueryable<Projeto> GetProjeto()
        {
            return db.Projeto;
        }

        // GET: api/Projetoes/5
        [ResponseType(typeof(Projeto))]
        public async Task<IHttpActionResult> GetProjeto(int id)
        {
            Projeto projeto = await db.Projeto.FindAsync(id);
            if (projeto == null)
            {
                return NotFound();
            }

            return Ok(projeto);
        }

        // PUT: api/Projetoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProjeto(int id, Projeto projeto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != projeto.Id)
            {
                return BadRequest();
            }

            db.Entry(projeto).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProjetoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Projetoes
        [ResponseType(typeof(Projeto))]
        public async Task<IHttpActionResult> PostProjeto(Projeto projeto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Projeto.Add(projeto);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProjetoExists(projeto.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = projeto.Id }, projeto);
        }

        // DELETE: api/Projetoes/5
        [ResponseType(typeof(Projeto))]
        public async Task<IHttpActionResult> DeleteProjeto(int id)
        {
            Projeto projeto = await db.Projeto.FindAsync(id);
            if (projeto == null)
            {
                return NotFound();
            }

            db.Projeto.Remove(projeto);
            await db.SaveChangesAsync();

            return Ok(projeto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProjetoExists(int id)
        {
            return db.Projeto.Count(e => e.Id == id) > 0;
        }
    }
}