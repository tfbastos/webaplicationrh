﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebApplicationRh;
using WebApplicationRh.Models;

namespace WebApplicationRh.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FormacaoAcademicaController : ApiController
    {
        private RhEntityDataModel db = new RhEntityDataModel();

        // GET: api/FormacaoAcademicas
        public IQueryable<FormacaoAcademica> GetFormacaoAcademica()
        {
            return db.FormacaoAcademica;
        }

        // GET: api/FormacaoAcademicas/5
        [ResponseType(typeof(FormacaoAcademica))]
        public async Task<IHttpActionResult> GetFormacaoAcademica(int id)
        {
            FormacaoAcademica formacaoAcademica = await db.FormacaoAcademica.FindAsync(id);
            if (formacaoAcademica == null)
            {
                return NotFound();
            }

            return Ok(formacaoAcademica);
        }

        // PUT: api/FormacaoAcademicas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutFormacaoAcademica(int id, FormacaoAcademica formacaoAcademica)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != formacaoAcademica.Id)
            {
                return BadRequest();
            }

            db.Entry(formacaoAcademica).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FormacaoAcademicaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FormacaoAcademicas
        [ResponseType(typeof(FormacaoAcademica))]
        public async Task<IHttpActionResult> PostFormacaoAcademica(FormacaoAcademica formacaoAcademica)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.FormacaoAcademica.Add(formacaoAcademica);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FormacaoAcademicaExists(formacaoAcademica.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = formacaoAcademica.Id }, formacaoAcademica);
        }

        // DELETE: api/FormacaoAcademicas/5
        [ResponseType(typeof(FormacaoAcademica))]
        public async Task<IHttpActionResult> DeleteFormacaoAcademica(int id)
        {
            FormacaoAcademica formacaoAcademica = await db.FormacaoAcademica.FindAsync(id);
            if (formacaoAcademica == null)
            {
                return NotFound();
            }

            db.FormacaoAcademica.Remove(formacaoAcademica);
            await db.SaveChangesAsync();

            return Ok(formacaoAcademica);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FormacaoAcademicaExists(int id)
        {
            return db.FormacaoAcademica.Count(e => e.Id == id) > 0;
        }
    }
}