﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using System.Web.Script.Services;
using System.Web.Services;
using WebApplicationRh;
using WebApplicationRh.Models;
using WebApplicationRh.Recursos;

namespace WebApplicationRh.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api")]
    public class PessoaHabilidadeController : ApiController
    {
        private RhEntityDataModel db = new RhEntityDataModel();

        // GET: api/PessoaHabilidades
        public IQueryable<PessoaHabilidades> GetPessoaHabilidades()
        {
            return db.PessoaHabilidades;
        }

        // GET: api/PessoaHabilidades/5
        [ResponseType(typeof(PessoaHabilidades))]
        public async Task<IHttpActionResult> GetPessoaHabilidades(int id)
        {
            PessoaHabilidades pessoaHabilidades = await db.PessoaHabilidades.FindAsync(id);
            if (pessoaHabilidades == null)
            {
                return NotFound();
            }

            return Ok(pessoaHabilidades);
        }

        // PUT: api/PessoaHabilidades/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPessoaHabilidades(int id, PessoaHabilidades pessoaHabilidades)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pessoaHabilidades.PessoaId)
            {
                return BadRequest();
            }

            db.Entry(pessoaHabilidades).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PessoaHabilidadesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PessoaHabilidades
        [ResponseType(typeof(PessoaHabilidades))]
        public async Task<IHttpActionResult> PostPessoaHabilidades(PessoaHabilidades pessoaHabilidades)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PessoaHabilidades.Add(pessoaHabilidades);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PessoaHabilidadesExists(pessoaHabilidades.PessoaId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pessoaHabilidades.PessoaId }, pessoaHabilidades);
        }

        // DELETE: api/PessoaHabilidades/5
        [ResponseType(typeof(PessoaHabilidades))]
        public async Task<IHttpActionResult> DeletePessoaHabilidades(int id)
        {
            PessoaHabilidades pessoaHabilidades = await db.PessoaHabilidades.FindAsync(id);
            if (pessoaHabilidades == null)
            {
                return NotFound();
            }

            db.PessoaHabilidades.Remove(pessoaHabilidades);
            await db.SaveChangesAsync();

            return Ok(pessoaHabilidades);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PessoaHabilidadesExists(int id)
        {
            return db.PessoaHabilidades.Count(e => e.PessoaId == id) > 0;
        }
    }
}