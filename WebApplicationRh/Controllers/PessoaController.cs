﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using System.Web.Script.Services;
using System.Web.Services;
using WebApplicationRh;
using WebApplicationRh.Models;
using WebApplicationRh.Recursos;

namespace WebApplicationRh.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api")]
    public class PessoaController : ApiController
    {
        private RhEntityDataModel db = new RhEntityDataModel();

        [HttpGet]
        [Route("PessoaDetalhe")]
        [ResponseType(typeof(Pessoa))]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[WebMethod()]
        public async Task<IHttpActionResult> GetListaPessoa(int id)
        {

            if (id != 0)
            {
                var retornoJson = new PessoaRecursos().DetalharPessoa(id);

                              return Ok(retornoJson);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("ListarPessoasFiltro")]
        [ResponseType(typeof(Pessoa))]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[WebMethod()]
        public async Task<IHttpActionResult> GetListaPessoa([FromUri]IEnumerable<int> ids)
        {
         
            if (ids != null)
            {
               var retornoJson = new PessoaRecursos().ListarPessoas(ids);

                //var teste = retornoJson.Replace(@"\", " ");

                return Ok(retornoJson);
            }
            return NotFound();
        }


        // GET: api/Pessoas
        public IQueryable<Pessoa> GetPessoa()
        {
            return db.Pessoa;
        }

        // GET: api/Pessoas/5
        [ResponseType(typeof(Pessoa))]
        public async Task<IHttpActionResult> GetPessoa(int id)
        {
            Pessoa pessoa = await db.Pessoa.FindAsync(id);
            if (pessoa == null)
            {
                return NotFound();
            }

            return Ok(pessoa);
        }

        // PUT: api/Pessoas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPessoa(int id, Pessoa pessoa)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pessoa.Id)
            {
                return BadRequest();
            }

            db.Entry(pessoa).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PessoaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Pessoas
        [ResponseType(typeof(Pessoa))]
        public async Task<IHttpActionResult> PostPessoa(Pessoa pessoa)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Pessoa.Add(pessoa);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PessoaExists(pessoa.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pessoa.Id }, pessoa);
        }

        // DELETE: api/Pessoas/5
        [ResponseType(typeof(Pessoa))]
        public async Task<IHttpActionResult> DeletePessoa(int id)
        {
            Pessoa pessoa = await db.Pessoa.FindAsync(id);
            if (pessoa == null)
            {
                return NotFound();
            }

            db.Pessoa.Remove(pessoa);
            await db.SaveChangesAsync();

            return Ok(pessoa);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PessoaExists(int id)
        {
            return db.Pessoa.Count(e => e.Id == id) > 0;
        }
    }
}