﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebApplicationRh;
using WebApplicationRh.Models;

namespace WebApplicationRh.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HabilidadesController : ApiController
    {
        private RhEntityDataModel db = new RhEntityDataModel();

        // GET: api/Habilidades
        public IQueryable<Habilidades> GetHabilidades()
        {
            return db.Habilidades;
        }

        // GET: api/Habilidades/5
        [ResponseType(typeof(Habilidades))]
        public async Task<IHttpActionResult> GetHabilidades(int id)
        {
            Habilidades habilidades = await db.Habilidades.FindAsync(id);
            if (habilidades == null)
            {
                return NotFound();
            }

            return Ok(habilidades);
        }

        // PUT: api/Habilidades/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutHabilidades(int id, Habilidades habilidades)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != habilidades.Id)
            {
                return BadRequest();
            }

            db.Entry(habilidades).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HabilidadesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Habilidades
        [ResponseType(typeof(Habilidades))]
        public async Task<IHttpActionResult> PostHabilidades(Habilidades habilidades)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Habilidades.Add(habilidades);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (HabilidadesExists(habilidades.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = habilidades.Id }, habilidades);
        }

        // DELETE: api/Habilidades/5
        [ResponseType(typeof(Habilidades))]
        public async Task<IHttpActionResult> DeleteHabilidades(int id)
        {
            Habilidades habilidades = await db.Habilidades.FindAsync(id);
            if (habilidades == null)
            {
                return NotFound();
            }

            db.Habilidades.Remove(habilidades);
            await db.SaveChangesAsync();

            return Ok(habilidades);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HabilidadesExists(int id)
        {
            return db.Habilidades.Count(e => e.Id == id) > 0;
        }
    }
}