﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebApplicationRh.Models;

namespace WebApplicationRh.Recursos
{
    public class PessoaRecursos
    {

        private RhEntityDataModel db = new RhEntityDataModel();

        Pessoa pessoa = new Pessoa();
        Habilidades habilidade = new Habilidades();
        PessoaHabilidades pessoaHabilidade = new PessoaHabilidades();

        public IEnumerable<Object> ListarPessoas([FromUri]IEnumerable<int> filtrosIds)
        {
            try
            {
                var pessoas = (from p in db.Pessoa
                               join ph in db.PessoaHabilidades on p.Id equals ph.PessoaId
                               join h in db.Habilidades on ph.HabilidadeId equals h.Id
                               join ep in db.ExperienciaProfissional on p.Id equals ep.PessoaId
                               join c in db.Cargo on ep.CargoId equals c.Id

                               where filtrosIds.Contains(h.Id)

                               select new
                               {
                                   PessoaId = p.Id,
                                   PessoaNome = p.Nome,
                                   CargoNome = c.Nome,
                                   PessoaNacionalidade = p.Naturalidade,
                                   PessoaNaturalidade = p.Naturalidade
                               })
                               .Distinct()
                               .Take(100)
                               .ToList();

                return pessoas;

            }
            catch (Exception e)
            {
                throw;
            }



        }

        public IEnumerable<Object> DetalharPessoa(int id)
        {
            try
            {
                #region 
                var result =
                    (from pessoa in db.Pessoa
                     join pessoaHabilidade in db.PessoaHabilidades on pessoa.Id equals pessoaHabilidade.PessoaId
                     join experienciaProfissional in db.ExperienciaProfissional on pessoa.Id equals experienciaProfissional.PessoaId
                     join formacaoAcademica in db.FormacaoAcademica on pessoa.Id equals formacaoAcademica.PessoaId
                     join certificacao in db.Certificacoes on pessoa.Id equals certificacao.PessoaId
                     join habilidade in db.Habilidades on pessoaHabilidade.HabilidadeId equals habilidade.Id
                     join cargo in db.Cargo on experienciaProfissional.CargoId equals cargo.Id
                     join empresa in db.Empresa on experienciaProfissional.EmpresaId equals empresa.Id
                     join projeto in db.Projeto on empresa.Id equals projeto.EmpresaId
                     join pessoaHabilidadeRecomedacoes in db.PessoaHabilidadeRecomendacoes
                        on pessoa.Id equals pessoaHabilidadeRecomedacoes.PessoaId

                     where pessoa.Id.Equals(id)

                     select new
                     {
                         id = pessoa.Id,
                         nome = pessoa.Nome,
                         sobreNome = pessoa.Sobrenome,
                         apresentacao = pessoa.Apresentacao,
                         nacionalidade = pessoa.Nacionalidade,
                         naturalidade = pessoa.Naturalidade,
                         cargoAtual = pessoa.ExperienciaProfissional.Select(cargo => cargo.Cargo.Nome),
                         empresaAtual = pessoa.ExperienciaProfissional.Select(empresa => empresa.Empresa.Nome),
                         certificacacoes = pessoa.Certificacoes,
                         cargos = pessoa.ExperienciaProfissional,
                         formacoes = pessoa.FormacaoAcademica,
                         habilidades = pessoaHabilidade,
                         empresa = pessoa.ExperienciaProfissional.Select(empresa => empresa.Empresa.Projeto),
                         recomendacoes = pessoa.PessoaHabilidades.Select(recomendacoes => recomendacoes.PessoaHabilidadeRecomendacoes)

                     }).Take(1).ToList();
                #endregion


                return result;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}