namespace WebApplicationRh
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using WebApplicationRh.Models;

    public partial class RhEntityDataModel : DbContext
    {
        public RhEntityDataModel()
            : base("name=RhEntityDataModel")
        {
        }

        public virtual DbSet<Cargo> Cargo { get; set; }
        public virtual DbSet<Certificacoes> Certificacoes { get; set; }
        public virtual DbSet<Contatos> Contatos { get; set; }
        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<ExperienciaProfissional> ExperienciaProfissional { get; set; }
        public virtual DbSet<FormacaoAcademica> FormacaoAcademica { get; set; }
        public virtual DbSet<Habilidades> Habilidades { get; set; }
        public virtual DbSet<Pessoa> Pessoa { get; set; }
        public virtual DbSet<PessoaHabilidadeRecomendacoes> PessoaHabilidadeRecomendacoes { get; set; }
        public virtual DbSet<PessoaHabilidades> PessoaHabilidades { get; set; }
        public virtual DbSet<Projeto> Projeto { get; set; }
        public virtual DbSet<ProjetoParticipantes> ProjetoParticipantes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cargo>()
                .Property(e => e.Nome)
                .IsUnicode(false);

            modelBuilder.Entity<Cargo>()
                .Property(e => e.Descricao)
                .IsUnicode(false);

            modelBuilder.Entity<Cargo>()
                .HasMany(e => e.ExperienciaProfissional)
                .WithRequired(e => e.Cargo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Certificacoes>()
                .Property(e => e.Titulo)
                .IsUnicode(false);

            modelBuilder.Entity<Certificacoes>()
                .Property(e => e.Orgao)
                .IsUnicode(false);

            modelBuilder.Entity<Contatos>()
                .Property(e => e.TipoContato)
                .IsUnicode(false);

            modelBuilder.Entity<Contatos>()
                .Property(e => e.Contato)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Nome)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.AreaAtuacao)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Descricao)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .HasMany(e => e.ExperienciaProfissional)
                .WithRequired(e => e.Empresa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Empresa>()
                .HasMany(e => e.Projeto)
                .WithRequired(e => e.Empresa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ExperienciaProfissional>()
                .Property(e => e.Localidade)
                .IsUnicode(false);

            modelBuilder.Entity<ExperienciaProfissional>()
                .Property(e => e.Titulo)
                .IsUnicode(false);

            modelBuilder.Entity<ExperienciaProfissional>()
                .Property(e => e.DescricaoAtividades)
                .IsUnicode(false);

            modelBuilder.Entity<FormacaoAcademica>()
                .Property(e => e.Instituicao)
                .IsUnicode(false);

            modelBuilder.Entity<FormacaoAcademica>()
                .Property(e => e.Formacao)
                .IsUnicode(false);

            modelBuilder.Entity<FormacaoAcademica>()
                .Property(e => e.AreaEstudo)
                .IsUnicode(false);

            modelBuilder.Entity<Habilidades>()
                .Property(e => e.Nome)
                .IsUnicode(false);

            modelBuilder.Entity<Habilidades>()
                .Property(e => e.Descricao)
                .IsUnicode(false);

            modelBuilder.Entity<Habilidades>()
                .HasMany(e => e.PessoaHabilidades)
                .WithRequired(e => e.Habilidades)
                .HasForeignKey(e => e.HabilidadeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Habilidades>()
                .HasMany(e => e.Projeto)
                .WithMany(e => e.Habilidades)
                .Map(m => m.ToTable("ProjetoHabilidades").MapLeftKey("HabilidadeId").MapRightKey("ProjetoId"));

            modelBuilder.Entity<Pessoa>()
                .Property(e => e.Nome)
                .IsUnicode(false);

            modelBuilder.Entity<Pessoa>()
                .Property(e => e.Sobrenome)
                .IsUnicode(false);

            modelBuilder.Entity<Pessoa>()
                .Property(e => e.Apresentacao)
                .IsUnicode(false);

            modelBuilder.Entity<Pessoa>()
                .Property(e => e.Nacionalidade)
                .IsUnicode(false);

            modelBuilder.Entity<Pessoa>()
                .Property(e => e.Naturalidade)
                .IsUnicode(false);

            modelBuilder.Entity<Pessoa>()
                .Property(e => e.Perfil)
                .IsUnicode(false);

            modelBuilder.Entity<Pessoa>()
                .HasMany(e => e.Certificacoes)
                .WithRequired(e => e.Pessoa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pessoa>()
                .HasMany(e => e.Contatos)
                .WithRequired(e => e.Pessoa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pessoa>()
                .HasMany(e => e.ExperienciaProfissional)
                .WithRequired(e => e.Pessoa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pessoa>()
                .HasMany(e => e.FormacaoAcademica)
                .WithRequired(e => e.Pessoa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pessoa>()
                .HasMany(e => e.PessoaHabilidades)
                .WithRequired(e => e.Pessoa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pessoa>()
                .HasMany(e => e.ProjetoParticipantes)
                .WithRequired(e => e.Pessoa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pessoa>()
                .HasMany(e => e.Habilidades)
                .WithMany(e => e.Pessoa)
                .Map(m => m.ToTable("PessoaInteresses").MapLeftKey("PessoaId").MapRightKey("HabilidadeId"));

            modelBuilder.Entity<PessoaHabilidadeRecomendacoes>()
                .Property(e => e.Recomendacao)
                .IsUnicode(false);

            modelBuilder.Entity<PessoaHabilidadeRecomendacoes>()
                .Property(e => e.PessoaRecomendacao)
                .IsUnicode(false);

            modelBuilder.Entity<PessoaHabilidades>()
                .HasMany(e => e.PessoaHabilidadeRecomendacoes)
                .WithRequired(e => e.PessoaHabilidades)
                .HasForeignKey(e => new { e.PessoaId, e.HabilidadeId })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Projeto>()
                .Property(e => e.Nome)
                .IsUnicode(false);

            modelBuilder.Entity<Projeto>()
                .Property(e => e.Descricao)
                .IsUnicode(false);

            modelBuilder.Entity<Projeto>()
                .HasMany(e => e.ProjetoParticipantes)
                .WithRequired(e => e.Projeto)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProjetoParticipantes>()
                .Property(e => e.Funcao)
                .IsUnicode(false);

            modelBuilder.Entity<ProjetoParticipantes>()
                .Property(e => e.Feedback)
                .IsUnicode(false);
        }
    }
}
