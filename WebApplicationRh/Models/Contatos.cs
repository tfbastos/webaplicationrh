namespace WebApplicationRh.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;

    //[Serializable]
    [DataContract]
    public partial class Contatos
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DataMember]
        public int Id { get; set; }

        public int PessoaId { get; set; }

        [StringLength(500)]
        [DataMember]
        public string TipoContato { get; set; }

        [StringLength(500)]
        [DataMember]
        public string Contato { get; set; }

        public virtual Pessoa Pessoa { get; set; }
    }
}
