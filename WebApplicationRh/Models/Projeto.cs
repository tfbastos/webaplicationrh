namespace WebApplicationRh.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;

    //[Serializable]
    [Table("Projeto")]
    [DataContract]
    public partial class Projeto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Projeto()
        {
            ProjetoParticipantes = new HashSet<ProjetoParticipantes>();
            Habilidades = new HashSet<Habilidades>();
        }

        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int EmpresaId { get; set; }

        [Required]
        [DataMember]
        [StringLength(500)]
        public string Nome { get; set; }

        [Required]
        [DataMember]
        [StringLength(5000)]
        public string Descricao { get; set; }

        [DataMember]
        [Column(TypeName = "date")]
        public DateTime DataInicio { get; set; }

        [DataMember]
        [Column(TypeName = "date")]
        public DateTime? DataFim { get; set; }

        public virtual Empresa Empresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProjetoParticipantes> ProjetoParticipantes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Habilidades> Habilidades { get; set; }
    }
}
