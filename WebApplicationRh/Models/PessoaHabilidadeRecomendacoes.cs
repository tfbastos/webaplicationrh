namespace WebApplicationRh.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;
    //[Serializable]
    [DataContract]
    public partial class PessoaHabilidadeRecomendacoes
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DataMember]
        public int Id { get; set; }

        public int PessoaId { get; set; }

        public int HabilidadeId { get; set; }

        [Required]
        [DataMember]
        [StringLength(500)]
        public string Recomendacao { get; set; }

        [Required]
        [DataMember]
        [StringLength(500)]
        public string PessoaRecomendacao { get; set; }

        [DataMember]
        public DateTime DataRecomendacao { get; set; }
               
        public virtual PessoaHabilidades PessoaHabilidades { get; set; }
    }
}
