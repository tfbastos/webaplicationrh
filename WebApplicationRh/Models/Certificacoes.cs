namespace WebApplicationRh.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;

    //[Serializable]
    [DataContract]
    public partial class Certificacoes
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int PessoaId { get; set; }

        [Required]
        [DataMember]
        [StringLength(500)]
        public string Titulo { get; set; }

        [Required]
        [DataMember]
        [StringLength(500)]
        public string Orgao { get; set; }

        [DataMember]
        [Column(TypeName = "date")]
        public DateTime DataInicio { get; set; }

        [DataMember]
        [Column(TypeName = "date")]
        public DateTime? DataFim { get; set; }

        public virtual Pessoa Pessoa { get; set; }

        public IEnumerable<Certificacoes> certificacoes {get;set;}
    }
}
