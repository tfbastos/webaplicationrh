namespace WebApplicationRh.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;

    //[Serializable]
    [DataContract]
    public partial class PessoaHabilidades
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PessoaHabilidades()
        {
            PessoaHabilidadeRecomendacoes = new HashSet<PessoaHabilidadeRecomendacoes>();
           
        }

        [Key]
        [DataMember]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        //[ForeignKey("Pessoa")]
        public int PessoaId { get; set; }

        [Key]
        [DataMember]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("Habilidades")]
        public int HabilidadeId { get; set; }
        
        public virtual Habilidades Habilidades { get; set; }
        
        public virtual Pessoa Pessoa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PessoaHabilidadeRecomendacoes> PessoaHabilidadeRecomendacoes { get; set; }

    }
}
