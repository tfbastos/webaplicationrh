namespace WebApplicationRh.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;

    //[Serializable]
    [DataContract]
    [Table("FormacaoAcademica")]
    public partial class FormacaoAcademica
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DataMember]
        public int Id { get; set; }

        public int PessoaId { get; set; }

        [Required]
        [DataMember]
        [StringLength(500)]
        public string Instituicao { get; set; }

        [Required]
        [DataMember]
        [StringLength(500)]
        public string Formacao { get; set; }

        [Required]
        [DataMember]
        [StringLength(500)]
        public string AreaEstudo { get; set; }

        [DataMember]
        public int AnoInicio { get; set; }

        [DataMember]
        public int? AnoConclusao { get; set; }

        public virtual Pessoa Pessoa { get; set; }
    }
}
