namespace WebApplicationRh.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;

    //[Serializable]
    [Table("ExperienciaProfissional")]
    [DataContract]
    public partial class ExperienciaProfissional
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DataMember]
        public int Id { get; set; }

        public int PessoaId { get; set; }

        public int CargoId { get; set; }

        public int EmpresaId { get; set; }

        [DataMember]
        [StringLength(500)]
        public string Localidade { get; set; }

        [DataMember]
        [Column(TypeName = "date")]
        public DateTime DataInicio { get; set; }

        [DataMember]
        [Column(TypeName = "date")]
        public DateTime? DataFim { get; set; }

        [DataMember]
        [StringLength(500)]
        public string Titulo { get; set; }

        [DataMember]
        [StringLength(5000)]
        public string DescricaoAtividades { get; set; }

        public virtual Cargo Cargo { get; set; }

        public virtual Empresa Empresa { get; set; }

        public virtual Pessoa Pessoa { get; set; }
    }
}
