namespace WebApplicationRh.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;

    //[Serializable]
    [DataContract]
    [Table("Pessoa")]
    public partial class Pessoa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Pessoa()
        {
            Certificacoes = new HashSet<Certificacoes>();
            Contatos = new HashSet<Contatos>();
            ExperienciaProfissional = new HashSet<ExperienciaProfissional>();
            FormacaoAcademica = new HashSet<FormacaoAcademica>();
            PessoaHabilidades = new HashSet<PessoaHabilidades>();
            ProjetoParticipantes = new HashSet<ProjetoParticipantes>();
            Habilidades = new HashSet<Habilidades>();
        }

        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int Id { get; set; }

        [Required]
        [DataMember]
        [StringLength(500)]
        public string Nome { get; set; }

        [Required]
        [DataMember]
        [StringLength(500)]
        public string Sobrenome { get; set; }

        [DataMember]
        [StringLength(4000)]
        public string Apresentacao { get; set; }

        [DataMember]
        [StringLength(500)]
        public string Nacionalidade { get; set; }

        [DataMember]
        [StringLength(500)]
        public string Naturalidade { get; set; }

        [DataMember]
        [StringLength(500)]
        public string Perfil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Certificacoes> Certificacoes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Contatos> Contatos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExperienciaProfissional> ExperienciaProfissional { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormacaoAcademica> FormacaoAcademica { get; set; }

        //[DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PessoaHabilidades> PessoaHabilidades { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProjetoParticipantes> ProjetoParticipantes { get; set; }

        //[DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Habilidades> Habilidades { get; set; }
    }
}
