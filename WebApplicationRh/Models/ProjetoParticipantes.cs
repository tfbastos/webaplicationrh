namespace WebApplicationRh.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;

    //[Serializable]
    [DataContract]
    public partial class ProjetoParticipantes
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DataMember]
        public int ProjetoId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PessoaId { get; set; }

        [Required]
        [DataMember]
        [StringLength(500)]
        public string Funcao { get; set; }

        [DataMember]
        [StringLength(5000)]
        public string Feedback { get; set; }

        [DataMember]
        [Column(TypeName = "date")]
        public DateTime DataInicio { get; set; }

        [DataMember]
        [Column(TypeName = "date")]
        public DateTime? DataFim { get; set; }

        public virtual Pessoa Pessoa { get; set; }

        public virtual Projeto Projeto { get; set; }
    }
}
